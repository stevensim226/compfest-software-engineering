from django.urls import path
from rest_api.api.views import meme_list_view, save_meme_view, detail_meme_view, delete_meme_view, check_meme_is_saved_view

app_name = "rest_api"

urlpatterns = [
    path("",meme_list_view,name="detail"),
    path("create",save_meme_view,name="save_meme"),
    path("delete",delete_meme_view,name="delete"),
    path("check",check_meme_is_saved_view,name="check"),
    path("<id>",detail_meme_view,name="detail")
]